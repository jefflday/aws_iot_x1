/*
 * Copyright (c) 2024 JLD Technology Solutions, LLC
 *
 */
#include <zephyr/kernel.h>
#include <zephyr/sys/reboot.h>
#include <zephyr/dfu/mcuboot.h>
#include <zephyr/logging/log.h>
#include <zephyr/logging/log_ctrl.h>
#include <zephyr/net/conn_mgr_connectivity.h>
#include <zephyr/net/conn_mgr_monitor.h>
#include <net/aws_iot.h>
#include <stdio.h>
#include <stdlib.h>
#include <hw_id.h>
#include <modem/modem_info.h>
#include <zephyr/device.h>
#include <zephyr/devicetree.h>
#include <zephyr/drivers/gpio.h>

//#include <zephyr/ztest.h>

#include "json_payload.h"
#include "iaq_app.h"

extern const struct gpio_dt_spec button;
extern const struct gpio_dt_spec led_0;
extern const struct gpio_dt_spec led_1;



/* Register log module */
LOG_MODULE_REGISTER(iaq_app, CONFIG_AWS_IOT_SAMPLE_LOG_LEVEL);

/* IAQ Threads */
#define STACKSIZE 1024

#define THREAD0_PRIORITY 6
#define THREAD1_PRIORITY 7

void thread0(void)
{
	
	LOG_INF("****IAQ Thread0 Started.****");
	while (1) {
          LOG_WRN("Hello, I am thread 0\n");
		  k_msleep(10000);
	}
}

void thread1(void)
{
	uint32_t thread1_run_counter = 0;
	bool prev_button_state = 0;
	bool button_state = 0;

	LOG_INF("****IAQ Thread1 Started.****");

	while (1) {

		thread1_run_counter++;

		
		button_state = gpio_pin_get_dt(&button);

		if( button_state != prev_button_state )
		{
			LOG_WRN("Button changed to %d\n", button_state);
		}

		prev_button_state = button_state;


		if( thread1_run_counter % 200 == 0 )
		{
            LOG_INF("Hello, I am thread 1\n");
		}
		  
		k_msleep(5);
	}
}

K_THREAD_DEFINE(thread0_id, STACKSIZE, thread0, NULL, NULL, NULL,
		THREAD0_PRIORITY, 0, 1);
K_THREAD_DEFINE(thread1_id, STACKSIZE, thread1, NULL, NULL, NULL,
		THREAD1_PRIORITY, 0, 2);


