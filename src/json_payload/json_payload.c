/*
 * Copyright (c) 2023 Nordic Semiconductor ASA
 *
 * SPDX-License-Identifier: LicenseRef-Nordic-5-Clause
 */

#include <zephyr/types.h>
#include <zephyr/logging/log.h>
#include <zephyr/data/json.h>

#include "json_payload.h"

/* Register log module */
LOG_MODULE_REGISTER(json_payload, CONFIG_AWS_IOT_SAMPLE_LOG_LEVEL);

int json_payload_construct(char *message, size_t size, struct payload *payload)
{
	int err;
	const struct json_obj_descr parameters[] = {
		JSON_OBJ_DESCR_PRIM_NAMED(struct payload, "uptime",
					  state.reported.uptime, JSON_TOK_NUMBER),
		JSON_OBJ_DESCR_PRIM_NAMED(struct payload, "app_version",
					  state.reported.app_version, JSON_TOK_STRING),
#if defined(CONFIG_MODEM_INFO)
		JSON_OBJ_DESCR_PRIM_NAMED(struct payload, "modem_version",
					  state.reported.modem_version, JSON_TOK_STRING)
#endif
		JSON_OBJ_DESCR_PRIM_NAMED(struct payload, "device_name",
					  state.reported.device_name, JSON_TOK_STRING),
		JSON_OBJ_DESCR_PRIM_NAMED(struct payload, "device_id",
					  state.reported.device_id, JSON_TOK_NUMBER),
		JSON_OBJ_DESCR_PRIM_NAMED(struct payload, "firmware_rev",
					  state.reported.firmware_rev, JSON_TOK_NUMBER),
		JSON_OBJ_DESCR_PRIM_NAMED(struct payload, "hardware_rev",
					  state.reported.hardware_rev, JSON_TOK_NUMBER),
		JSON_OBJ_DESCR_PRIM_NAMED(struct payload, "control",
					  state.reported.control, JSON_TOK_NUMBER),
		JSON_OBJ_DESCR_PRIM_NAMED(struct payload, "status",
					  state.reported.status, JSON_TOK_NUMBER),
		JSON_OBJ_DESCR_PRIM_NAMED(struct payload, "batt_voltage",
					  state.reported.batt_voltage, JSON_TOK_NUMBER),
		JSON_OBJ_DESCR_PRIM_NAMED(struct payload, "temperature_avg",
					  state.reported.temperature_avg, JSON_TOK_NUMBER),
		JSON_OBJ_DESCR_PRIM_NAMED(struct payload, "humidity_avg",
					  state.reported.humidity_avg, JSON_TOK_NUMBER),
/**/
		JSON_OBJ_DESCR_PRIM_NAMED(struct payload, "car_dioxide_avg",
					  state.reported.car_dioxide_avg, JSON_TOK_NUMBER),
		JSON_OBJ_DESCR_PRIM_NAMED(struct payload, "car_monoxide_avg",
					  state.reported.car_monoxide_avg, JSON_TOK_NUMBER),
		JSON_OBJ_DESCR_PRIM_NAMED(struct payload, "ozone_chlor_avg",
					  state.reported.ozone_chlor_avg, JSON_TOK_NUMBER),
		JSON_OBJ_DESCR_PRIM_NAMED(struct payload, "voc_avg",
					  state.reported.voc_avg, JSON_TOK_NUMBER),
		JSON_OBJ_DESCR_PRIM_NAMED(struct payload, "nox_avg",
					  state.reported.nox_avg, JSON_TOK_NUMBER),
		JSON_OBJ_DESCR_PRIM_NAMED(struct payload, "pm_2_5_avg",
					  state.reported.pm_2_5_avg, JSON_TOK_NUMBER),
		JSON_OBJ_DESCR_PRIM_NAMED(struct payload, "pm_10_avg",
					  state.reported.pm_10_avg, JSON_TOK_NUMBER),
		JSON_OBJ_DESCR_PRIM_NAMED(struct payload, "radon",
					  state.reported.radon, JSON_TOK_NUMBER),
		JSON_OBJ_DESCR_PRIM_NAMED(struct payload, "radon_avg",
					  state.reported.radon_avg, JSON_TOK_NUMBER),
		JSON_OBJ_DESCR_PRIM_NAMED(struct payload, "baro_pres_avg",
					  state.reported.baro_pres_avg, JSON_TOK_NUMBER),
		JSON_OBJ_DESCR_PRIM_NAMED(struct payload, "amb_light",
					  state.reported.amb_light, JSON_TOK_NUMBER),
		JSON_OBJ_DESCR_PRIM_NAMED(struct payload, "amb_sound",
					  state.reported.amb_sound, JSON_TOK_NUMBER),
		JSON_OBJ_DESCR_PRIM_NAMED(struct payload, "wifi_rssi",
					  state.reported.wifi_rssi, JSON_TOK_NUMBER),
/**/
		JSON_OBJ_DESCR_PRIM_NAMED(struct payload, "spare_1",
					  state.reported.spare_1, JSON_TOK_NUMBER),
		JSON_OBJ_DESCR_PRIM_NAMED(struct payload, "spare_2",
					  state.reported.spare_2, JSON_TOK_NUMBER),
	};
	const struct json_obj_descr reported[] = {
		JSON_OBJ_DESCR_OBJECT_NAMED(struct payload, "reported", state.reported,
					    parameters),
	};
	const struct json_obj_descr root[] = {
		JSON_OBJ_DESCR_OBJECT(struct payload, state, reported),
	};

	err = json_obj_encode_buf(root, ARRAY_SIZE(root), payload, message, size);
	if (err) {
		LOG_ERR("json_obj_encode_buf, error: %d", err);
		return err;
	}

	return 0;
}
