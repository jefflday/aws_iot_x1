/*
 * Copyright (c) 2023 Nordic Semiconductor ASA
 *
 * SPDX-License-Identifier: LicenseRef-Nordic-5-Clause
 */

#include <zephyr/types.h>

/* Structure used to populate and describe the JSON payload sent to AWS IoT. */
struct payload {
	struct {
		struct {
			const char *app_version;
			const char *modem_version;
			const char * device_name;
			uint32_t device_id;
			uint32_t uptime;
			//uint32_t time;

			//uint32_t iaq_data[20];
			uint32_t firmware_rev;
			uint32_t hardware_rev;
			//uint32_t serial_number;
			uint32_t control;
			uint32_t status;
			uint32_t batt_voltage;
			uint32_t temperature_avg;
			uint32_t humidity_avg;
			/**/
			uint32_t car_dioxide_avg;
			uint32_t car_monoxide_avg;
			uint32_t ozone_chlor_avg;
			uint32_t voc_avg;
			uint32_t nox_avg;
			uint32_t pm_2_5_avg;
			uint32_t pm_10_avg;
			uint32_t radon;
			uint32_t radon_avg;
			uint32_t baro_pres_avg;
			uint32_t amb_light;
			uint32_t amb_sound;
			uint32_t wifi_rssi;
			/**/
			uint32_t spare_1;
			uint32_t spare_2;
		} reported;
	} state;
};

/* @brief Construct a JSON message string.
 *
 * @param[out] message Pointer to a buffer that the JSON string is written to.
 * @param[in]  size    Size of the output buffer, message.
 * @param[in]  payload Pointer to a payload structure that will be used
 *	       to populate the JSON message.
 *
 * @return 0 on success, otherwise a negative value is returned.
 */
int json_payload_construct(char *message, size_t size, struct payload *payload);
